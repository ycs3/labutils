import camera
import cv2
import laceconfig
import scene
import win

scr = win.Win3D()
inp = win.Input()

stream = camera.KinectImageStream("../llgm10_img")
while True:
    color, depth = stream.getFrame()
    cv2.imshow("COLOR", color)
    cv2.imshow("DEPTH", depth)
    k = cv2.waitKey(10)
    if k == 27:
        break

    act = inp.event()
    scr.event(act)
    if len(act) > 0:
        print act
    scr.clear(True)
    cropped_color, pointcloud_3d = scene.draw_scene(color, depth, laceconfig.trans2_first, show_gl = True)
    scr.update()
